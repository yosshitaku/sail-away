const Utils = {
  arrayMoveAt: function <T>(array: T[], from: number, to: number) {
    if (from === to || from > array.length - 1 || to > array.length - 1) {
      return array;
    }

    const value = array[from];
    const tail = array.slice(from + 1);

    array.splice(from);

    Array.prototype.push.apply(array, tail);

    array.splice(to, 0, value);

    return array;
  },
};

export default Utils;
