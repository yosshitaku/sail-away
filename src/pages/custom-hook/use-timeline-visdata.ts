import { useEffect, useRef, useState } from "react";
import { DummyData } from "../../dummy-data";
import csvToTimelineElement from "../../logic/timeline/csv-to-timeline-element";
import timelineToVisdata from "../../logic/timeline/timeline-to-visdata";
import visDataToMyTimeline from "../../logic/timeline/visdata-to-my-timeline";
import {
  Conditions,
  GroupOrderTable,
  Timeline,
} from "../../model/timeline.model";

const useTimelineVisData = () => {
  const [timeline, setTimeline] = useState<Timeline | null>(null);
  const csvLines = useRef<string[]>([]);
  const orderTable = useRef<GroupOrderTable>({});

  const genTimeline = (lines: string[], newConditions: Conditions) => {
    const { min, max, timelines, headers, conditions } = csvToTimelineElement(
      [...lines],
      "time",
      newConditions
    );
    const visData = timelineToVisdata({
      timelines,
      headers,
      xAxisLabel: "time",
    });
    const timeline = visDataToMyTimeline({
      id: "my-timeline",
      visData,
      range: { max, min },
      conditions,
    });

    const newGroupOrderTable: GroupOrderTable = {};
    timeline.groups.forEach((group) => {
      newGroupOrderTable[group.value.toString()] = group.content as string;
    });

    return { newGroupOrderTable, timeline };
  };

  const handleChangeNoDisplayFileInput = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (e.target.files === null || e.target.files.length === 0) {
      return;
    }

    const file = e.target.files[0];

    const fileReader = new FileReader();
    fileReader.readAsText(file);

    fileReader.onload = () => {
      const csv = fileReader.result;

      if (typeof csv === "string") {
        const lines = csv.split(/\n/).filter((line) => line.trim() !== "");
        csvLines.current = lines;
        const { timeline, newGroupOrderTable } = genTimeline(lines, {});
        orderTable.current = newGroupOrderTable;
        setTimeline(timeline);
      }
    };
  };

  const handleGenDummyData = () => {
    const csv = DummyData;
    const lines = csv.split(/\n/).filter((line) => line.trim() !== "");
    csvLines.current = lines;
    const { timeline, newGroupOrderTable } = genTimeline(lines, {});
    orderTable.current = newGroupOrderTable;
    setTimeline(timeline);
  };

  const updateTimeline = (newConditions: Conditions) => {
    const { timeline, newGroupOrderTable } = genTimeline(
      csvLines.current,
      newConditions
    );
    orderTable.current = newGroupOrderTable;
    setTimeline(timeline);
  };

  return {
    timeline,
    handleChangeNoDisplayFileInput,
    handleGenDummyData,
    updateTimeline,
  };
};

export default useTimelineVisData;
