import {
  IonButton,
  IonContent,
  IonHeader,
  IonModal,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useEffect, useRef, useState } from "react";
import ConditionForm, {
  Data as ModalData,
} from "../components/ConditionForm/ConditionForm.parts";
import Loading from "../components/Loading/Loading.parts";
import Timeline from "../components/Timeline/Timeline.parts";
import {
  COMPARATION_VALUE_TYPES,
  CONDITION_TYPES,
} from "../logic/timeline/eval-formula";
import { Group } from "../model/timeline.model";
import useTimelineVisData from "./custom-hook/use-timeline-visdata";
import "./Home.css";

const Home: React.FC = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [modal, setModal] = useState<{
    isOpen: boolean;
    data: {
      selectedGroup: null | Group;
      comparisonValue: string | null;
      condition: typeof CONDITION_TYPES[keyof typeof CONDITION_TYPES] | null;
      comparisonValueTypes:
        | typeof COMPARATION_VALUE_TYPES[keyof typeof COMPARATION_VALUE_TYPES]
        | null;
    };
    label: string;
  }>({
    isOpen: false,
    data: {
      selectedGroup: null,
      comparisonValue: "",
      condition: "==",
      comparisonValueTypes: "string",
    },
    label: "",
  });
  const {
    timeline,
    handleChangeNoDisplayFileInput,
    handleGenDummyData,
    updateTimeline,
  } = useTimelineVisData();

  const handleClickFileLoadButton = () => {
    if (inputRef.current) {
      inputRef.current.click();
    }
  };

  useEffect(() => {
    console.log(`updated timeline`);
    console.log(timeline?.conditions);
  }, [timeline?.conditions]);

  const handleOnSelect = (label: string) => {
    console.log("selected group");
    console.log(timeline?.conditions);
    const group = timeline?.groups.find((g) => g.content === label);
    if (timeline && group) {
      setModal({
        isOpen: true,
        data: {
          selectedGroup: group,
          comparisonValue: `${timeline.conditions[label].comparisonValue}`,
          condition: timeline.conditions[label].condition,
          comparisonValueTypes: timeline.conditions[label].type,
        },
        label,
      });
    }
  };

  const onModalCancel = () => {
    setModal({
      isOpen: false,
      data: {
        selectedGroup: null,
        comparisonValue: null,
        condition: null,
        comparisonValueTypes: null,
      },
      label: "",
    });
  };

  const onModalUpdate = (label: string, newData: ModalData) => {
    if (timeline) {
      const newConditions = { ...timeline.conditions };

      let comparisonValue: number | string = newData.comparisonValue!;

      if (newData.comparisonValueTypes === "number") {
        comparisonValue = Number(comparisonValue);
      }

      newConditions[label] = {
        type: newData.comparisonValueTypes!,
        comparisonValue,
        condition: newData.condition!,
      };

      updateTimeline(newConditions);
    }

    setModal({
      isOpen: false,
      data: {
        selectedGroup: null,
        comparisonValue: null,
        condition: null,
        comparisonValueTypes: null,
      },
      label: "",
    });
  };

  return (
    <>
      <IonPage>
        <IonHeader>
          <IonToolbar color="primary">
            <IonTitle>Sail Away</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
          <IonRow className="ion-justify-content-center">
            <input
              type="file"
              accept=".csv"
              ref={inputRef}
              style={{ display: "none" }}
              onChange={handleChangeNoDisplayFileInput}
            />
            <IonButton
              type="button"
              color="success"
              expand="block"
              onClick={handleClickFileLoadButton}
            >
              .csv File Load
            </IonButton>
            <IonButton color="tertiary" onClick={handleGenDummyData}>
              Load Dummy Data
            </IonButton>
          </IonRow>
          <div className="ion-padding">
            {timeline && <Timeline {...timeline} onSelect={handleOnSelect} />}
          </div>
        </IonContent>
      </IonPage>
      <IonModal isOpen={modal.isOpen} backdropDismiss={false}>
        <IonContent>
          <ConditionForm
            label={modal.label}
            data={modal.data}
            onCancel={onModalCancel}
            onUpdate={onModalUpdate}
          />
        </IonContent>
      </IonModal>
    </>
  );
};

export default Home;
