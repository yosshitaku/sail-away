import React, { useRef, useEffect, useState } from "react";
import styled from "styled-components";
import * as VisTimeline from "vis-timeline/standalone";
import { debounce } from "lodash";
import { Timeline as TimelineProps } from "../../model/timeline.model";
import Loading from "../Loading/Loading.parts";

const TimelineGroupButton = ({
  id,
  label,
  onClick,
}: {
  id: string;
  label: string;
  onClick: () => void;
}): HTMLButtonElement => {
  const button = document.createElement("button");

  button.id = id;
  button.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512"><title>Build</title><path d="M393.87 190a32.1 32.1 0 01-45.25 0l-26.57-26.57a32.09 32.09 0 010-45.26L382.19 58a1 1 0 00-.3-1.64c-38.82-16.64-89.15-8.16-121.11 23.57-30.58 30.35-32.32 76-21.12 115.84a31.93 31.93 0 01-9.06 32.08L64 380a48.17 48.17 0 1068 68l153.86-167a31.93 31.93 0 0131.6-9.13c39.54 10.59 84.54 8.6 114.72-21.19 32.49-32 39.5-88.56 23.75-120.93a1 1 0 00-1.6-.26z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="32"/><circle cx="96" cy="416" r="16"/></svg>`;
  button.style.width = "30px";
  button.style.fontSize = "large";
  button.style.border = "none";
  button.style.color = "black";
  button.style.backgroundColor = "transparent";
  button.addEventListener("click", onClick);

  return button;
};

const Warapper = styled.div`
  .vis-item.brink-pink {
    background: rgba(207, 127, 145, 1);
    border-color: rgba(207, 127, 145, 1);
  }

  .vis-background .vis-group:nth-child(even) {
    background: #ffffff;
  }

  .vis-background .vis-group:nth-child(odd) {
    background: #fffafa;
  }
`;

type Group = VisTimeline.DataGroup & { value: number };

interface EventProps {
  onSelect: (label: string) => void;
}

type Props = EventProps & TimelineProps;

const Timeline: React.VFC<Props> = ({ id, items, groups, range, onSelect }) => {
  const [rendering, setRendering] = useState(false);
  const ref = useRef<HTMLDivElement>(null);
  const timelineRef = useRef<{ timeline: VisTimeline.Timeline | null }>({
    timeline: null,
  });

  const initTimeline = (current: HTMLDivElement) => {
    const options: VisTimeline.TimelineOptions = {
      type: "range",
      min: range.min,
      max: range.max,
      maxHeight: "100%",
      width: "100%",
      showCurrentTime: true,
      selectable: true,
      stack: false,
      verticalScroll: true,
      horizontalScroll: true,
      zoomKey: "ctrlKey",
      showMajorLabels: false,
      editable: true,
      groupEditable: true,
      groupOrder: function (a, b) {
        return a.value - b.value;
      },
      groupOrderSwap: function (a, b) {
        const v = a.value;
        a.value = b.value;
        b.value = v;
      },
      // format: {
      //   minorLabels(date: any) {
      //     console.log(date);
      //     return Math.round(date).toString();
      //   },
      // },
      tooltip: {
        followMouse: true,
        template(item, data) {
          return data ? data.content : "";
        },
      },
      groupTemplate: (group: Group) => {
        const container = document.createElement("div");
        container.style.display = "flex";
        container.style.alignItems = "center";

        if (group && group.content) {
          const label = document.createElement("span");
          label.innerHTML = group.content + " ";
          const button = TimelineGroupButton({
            id: "",
            label: "⚙",
            onClick: () => {
              onSelect(group.content as string);
            },
          });

          container.insertAdjacentElement("afterbegin", button);
          container.insertAdjacentElement("beforeend", label);
        }

        return container;
      },
    };

    timelineRef.current.timeline = new VisTimeline.Timeline(
      current,
      items,
      groups,
      options
    );

    timelineRef.current.timeline.on(
      "changed",
      debounce(() => {
        setRendering(false);
      }, 1500)
    );
  };

  useEffect(() => {
    if (ref.current) {
      setRendering(true);
      if (timelineRef.current.timeline === null) {
        initTimeline(ref.current);
      } else {
        timelineRef.current.timeline.destroy();
        initTimeline(ref.current);

        // timelineRef.current.timeline.setItems(items);
        // timelineRef.current.timeline.setGroups(groups);
        // timelineRef.current.timeline.setOptions({
        //   min: range.min,
        //   max: range.max,
        // });
        // timelineRef.current.timeline.redraw();
      }
    }
  }, [groups, items, range, ref]);

  return (
    <>
      {rendering && <Loading />}
      <Warapper id={id} ref={ref} />
    </>
  );
};

export default Timeline;
