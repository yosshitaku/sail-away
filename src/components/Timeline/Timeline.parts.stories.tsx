import { ComponentStory, ComponentMeta } from "@storybook/react";
import Timeline from "./Timeline.parts";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Parts/Timeline",
  component: Timeline,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof Timeline>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Timeline> = (args) => (
  <Timeline {...args} />
);

const COLORS = ["brink-pink", "summer-sky", "light-stable-blue", "sunshade"];

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  onSelect: (args) => {
    console.log(args);
  },
  items: [
    {
      id: "Item_Label_1",
      group: "Group_Label_1",
      content: "1-3",
      start: 1,
      end: 3,
      className: COLORS[0 % COLORS.length],
    },
    {
      id: "Item_Label_2",
      group: "Group_Label_2",
      content: "2-6",
      start: 2,
      end: 6,
      className: COLORS[1 % COLORS.length],
    },
    {
      id: "Item_Label_3",
      group: "Group_Label_3",
      content: "3-7",
      start: 3,
      end: 7,
      className: COLORS[2 % COLORS.length],
    },
    {
      id: "Item_Label_4",
      group: "Group_Label_4",
      content: "8-10",
      start: 8,
      end: 10,
      className: COLORS[3 % COLORS.length],
    },
  ],
  groups: [
    {
      id: "Group_Label_1",
      content: "ラベル 1",
      value: 1,
    },
    {
      id: "Group_Label_2",
      content: "ラベル 2",
      value: 2,
    },
    {
      id: "Group_Label_3",
      content: "ラベル 3",
      value: 3,
    },
    {
      id: "Group_Label_4",
      content: "ラベル 4",
      value: 4,
    },
  ],
  range: {
    min: 0,
    max: 100,
  },
};
