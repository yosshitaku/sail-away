import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import DataOverviewList from "./DataOverviewList.parts";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Parts/DataOverviewList",
  component: DataOverviewList,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof DataOverviewList>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof DataOverviewList> = (args) => (
  <DataOverviewList {...args} />
);

let list = [
  {
    label: "label 1",
    display: true,
  },
  {
    label: "label 2",
    display: false,
  },
  {
    label: "label 3",
    display: true,
  },
  {
    label: "label 4",
    display: true,
  },
  {
    label: "label 5",
    display: true,
  },
];

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  list,
  reorder: true,
  onChange: (newList) => {
    list = newList;
  },
};
