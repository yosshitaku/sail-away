import { ItemReorderEventDetail } from "@ionic/core";
import {
  IonCheckbox,
  IonItem,
  IonLabel,
  IonReorder,
  IonReorderGroup,
} from "@ionic/react";
import React from "react";
import Utils from "../../utils";

interface Item {
  label: string;
  display: boolean;
}

interface Props {
  reorder: boolean;
  list: Item[];
  onChange: (list: Item[]) => void;
}

const DataOverviewList: React.VFC<Props> = ({ reorder, list, onChange }) => {
  const doReorder = (event: CustomEvent<ItemReorderEventDetail>) => {
    const newList = Utils.arrayMoveAt(list, event.detail.from, event.detail.to);
    onChange(newList);
    event.detail.complete();
  };

  return (
    <IonReorderGroup disabled={!reorder} onIonItemReorder={doReorder}>
      {list.map((item) => {
        return (
          <IonItem>
            <IonCheckbox slot="start" />
            <IonLabel>{item.label}</IonLabel>
            <IonReorder slot="end" />
          </IonItem>
        );
      })}
    </IonReorderGroup>
  );
};

export default DataOverviewList;
