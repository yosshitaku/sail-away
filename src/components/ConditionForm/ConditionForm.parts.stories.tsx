import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import ConditionForm from "./ConditionForm.parts";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Parts/ConditionForm",
  component: ConditionForm,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof ConditionForm>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof ConditionForm> = (args) => (
  <ConditionForm {...args} />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  label: "Title",
  data: {
    selectedGroup: {
      id: 0,
      content: "test",
      value: 0,
    },
    comparisonValue: "",
    comparisonValueTypes: "string",
    condition: "==",
  },
  onCancel: () => {
    console.log("canceled");
  },
  onUpdate: (newData) => {
    console.log(`updated.`);
    console.log(newData);
  },
};
