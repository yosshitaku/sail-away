import { InputChangeEventDetail, SelectChangeEventDetail } from "@ionic/core";
import {
  IonButton,
  IonCol,
  IonGrid,
  IonInput,
  IonItem,
  IonLabel,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonText,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import {
  COMPARATION_VALUE_TYPES,
  CONDITIONS,
  CONDITION_TYPES,
} from "../../logic/timeline/eval-formula";
import { Group } from "../../model/timeline.model";

import "./ConditionForm.parts.scss";

export type Data = {
  selectedGroup: null | Group;
  comparisonValue: string | null;
  condition: typeof CONDITION_TYPES[keyof typeof CONDITION_TYPES] | null;
  comparisonValueTypes:
    | typeof COMPARATION_VALUE_TYPES[keyof typeof COMPARATION_VALUE_TYPES]
    | null;
};

export type Props = {
  data: Data;
  label: string;
  onCancel: () => void;
  onUpdate: (label: string, newData: Data) => void;
};

const ValidationResult = {
  NO_DATA: "値を入力してください。",
  NOT_NUMBER: "数値を入力してください。",
  VALID: "正常",
} as const;

const ConditionForm: React.VFC<Props> = ({
  label,
  data,
  onCancel,
  onUpdate,
}) => {
  const [displayData, setDisplayData] = useState<Data>({
    selectedGroup: {
      id: 0,
      content: "test",
      value: 0,
    },
    comparisonValue: "",
    comparisonValueTypes: "string",
    condition: "==",
  });

  useEffect(() => {
    setDisplayData(data);
  }, [data]);

  const onChangeCondition = (event: CustomEvent<SelectChangeEventDetail>) => {
    setDisplayData({
      ...displayData,
      condition: event.detail.value,
    });
  };

  const onChangeValueType = (event: CustomEvent<SelectChangeEventDetail>) => {
    setDisplayData({
      ...displayData,
      comparisonValueTypes: event.detail.value,
    });
  };

  const onChangeComparisonValue = (
    event: CustomEvent<InputChangeEventDetail>
  ) => {
    if (event.detail.value !== undefined && event.detail.value !== null) {
      setDisplayData({
        ...displayData,
        comparisonValue: event.detail.value,
      });
    }
  };

  const comparisonValueValidation = () => {
    if (
      !displayData.comparisonValue ||
      displayData.comparisonValue.length < 1
    ) {
      return ValidationResult.NO_DATA;
    }

    if (
      displayData.comparisonValueTypes === "number" &&
      isNaN(Number(displayData.comparisonValue))
    ) {
      return ValidationResult.NOT_NUMBER;
    }

    return ValidationResult.VALID;
  };

  const validation = comparisonValueValidation();

  return (
    <div className="form-container">
      <IonGrid>
        <IonRow className="ion-justify-content-center">
          <h3>{label}</h3>
        </IonRow>
        <IonRow>
          <IonCol>
            <IonItem>
              <IonLabel>Conditions</IonLabel>
              <IonSelect
                interface="popover"
                value={displayData.condition}
                onIonChange={onChangeCondition}
              >
                {CONDITIONS.map((c) => {
                  return (
                    <IonSelectOption key={c.name} value={c.name}>
                      {c.name}
                    </IonSelectOption>
                  );
                })}
              </IonSelect>
            </IonItem>
          </IonCol>
          <IonCol>
            <IonItem>
              <IonLabel>Value Type</IonLabel>
              <IonSelect
                interface="popover"
                value={displayData.comparisonValueTypes}
                onIonChange={onChangeValueType}
              >
                {Object.keys(COMPARATION_VALUE_TYPES).map((c) => {
                  return (
                    <IonSelectOption key={c} value={c}>
                      {c}
                    </IonSelectOption>
                  );
                })}
              </IonSelect>
            </IonItem>
          </IonCol>
        </IonRow>
        <IonRow className="ion-justify-content-center">
          <div className="formula">
            <span className="text">Formula :</span>
            <div className="box">
              <IonText color="tertiary">
                <span className="text">Value</span>
              </IonText>
            </div>
            <div className="box">
              <IonText color="danger">
                <span className="text">{data.condition}</span>
              </IonText>
            </div>
            <div className="box">
              <IonItem>
                <IonInput
                  type="text"
                  className="text"
                  value={displayData.comparisonValue}
                  onIonChange={onChangeComparisonValue}
                />
              </IonItem>
            </div>
          </div>
        </IonRow>
        {validation !== ValidationResult.VALID && (
          <IonRow className="ion-justify-content-center">
            <IonText color="danger">
              <p>{validation}</p>
            </IonText>
          </IonRow>
        )}
        <IonRow className="ion-justify-content-center">
          <IonButton color="danger" onClick={onCancel}>
            Cancel
          </IonButton>
          <IonButton
            color="success"
            disabled={validation !== ValidationResult.VALID}
            onClick={() => onUpdate(label, displayData)}
          >
            Update
          </IonButton>
        </IonRow>
      </IonGrid>
    </div>
  );
};

export default ConditionForm;
