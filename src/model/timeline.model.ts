import * as VisTimeline from "vis-timeline/standalone";
import {
  COMPARATION_VALUE_TYPES,
  CONDITION_TYPES,
} from "../logic/timeline/eval-formula";

export interface GroupOrderTable {
  [value: string]: string;
}

export interface Conditions {
  [label: string]: {
    type: typeof COMPARATION_VALUE_TYPES[keyof typeof COMPARATION_VALUE_TYPES];
    comparisonValue: string | number;
    condition: typeof CONDITION_TYPES[keyof typeof CONDITION_TYPES];
  };
}

export type Group = VisTimeline.DataGroup & { value: number };

export interface Timeline {
  id: string;
  items: VisTimeline.DataItem[];
  groups: Group[];
  range: {
    min: number | Date;
    max: number | Date;
  };
  conditions: Conditions;
}
