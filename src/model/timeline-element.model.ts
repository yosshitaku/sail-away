export interface TimelineElement {
  from: number | Date;
  to: number | Date;
  label: string;
}
