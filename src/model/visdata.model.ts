import { DataGroup, DataItem } from "vis-timeline";

export interface VisData {
  items: DataItem[];
  groups: DataGroup[];
  // headers: {
  //   label: string;
  //   checked: boolean;
  // }[];
}
