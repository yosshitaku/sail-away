export const COMPARATION_VALUE_TYPES = {
  string: "string",
  number: "number",
} as const;

export const CONDITION_TYPES = {
  "==": "==",
  ">=": ">=",
  "<=": "<=",
  ">": ">",
  "<": "<",
  "!=": "!=",
} as const;

export const CONDITIONS = [
  {
    name: CONDITION_TYPES["=="],
  },
  {
    name: CONDITION_TYPES[">="],
  },
  {
    name: CONDITION_TYPES["<="],
  },
  {
    name: CONDITION_TYPES[">"],
  },
  {
    name: CONDITION_TYPES["<"],
  },
  {
    name: CONDITION_TYPES["!="],
  },
] as const;

type Args = {
  type: typeof COMPARATION_VALUE_TYPES[keyof typeof COMPARATION_VALUE_TYPES];
  value: string;
  comparisonValue: string | number;
  condition: typeof CONDITION_TYPES[keyof typeof CONDITION_TYPES];
};

const evalFormula = ({
  value,
  type,
  condition,
  comparisonValue,
}: Args): boolean => {
  let a: string | number = value;

  if (type === "number") {
    a = Number(value);
    if (isNaN(a)) {
      throw new Error(`${value} is NaN.`);
    }
  }

  switch (condition) {
    case "==": {
      return value === comparisonValue;
    }
    case "!=": {
      return value !== comparisonValue;
    }
    case "<": {
      return value < comparisonValue;
    }
    case ">": {
      return value > comparisonValue;
    }
    case "<=": {
      return value <= comparisonValue;
    }
    case ">=": {
      return value >= comparisonValue;
    }
    default: {
      throw new Error(`${condition} is unknown condition.`);
    }
  }
};

export default evalFormula;
