import { DataGroupCollectionType, DataItemCollectionType } from "vis-timeline";
import { TimelineElement } from "../../model/timeline-element.model";
import { VisData } from "../../model/visdata.model";

const COLORS = ["brink-pink", "summer-sky", "light-stable-blue", "sunshade"];

const timelineToVisdata = ({
  timelines,
  headers,
  xAxisLabel,
}: {
  headers: string[];
  timelines: TimelineElement[];
  xAxisLabel: string;
}): VisData => {
  // let newHeaderOrder: { label: string; checked: boolean }[] = [];

  const items: DataItemCollectionType = timelines.map((data, i) => {
    return {
      id: (i + 1).toString(),
      group: data.label,
      content: `${data.from} 〜 ${data.to}`,
      start: data.from,
      end: data.to,
      className: COLORS[i % COLORS.length],
    };
  });

  const groups: DataGroupCollectionType = headers
    // .filter((data) => {
    //   return headerOrder.includes(data);
    // })
    // .sort((x, y) => {
    //   return headerOrder.indexOf(x) - headerOrder.indexOf(y);
    // })
    .filter((label) => label !== xAxisLabel)
    .map((label) => {
      return { id: label, content: label };
    });

  // if (headerOrder.length > 0) {
  //   newHeaderOrder = headers
  //     .filter((h) => {
  //       return headerOrder.includes(h);
  //     })
  //     .map((h) => {
  //       return {
  //         label: h,
  //         checked: true,
  //       };
  //     })
  //     .sort((x, y) => {
  //       return headerOrder.indexOf(x.label) - headerOrder.indexOf(y.label);
  //     });
  // } else {
  //   newHeaderOrder = headers.map((h) => {
  //     return {
  //       label: h,
  //       checked: true,
  //     };
  //   });
  // }

  return {
    items,
    groups,
    // headers: newHeaderOrder,
  };
};

export default timelineToVisdata;
