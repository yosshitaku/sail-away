import { TimelineElement } from "../../model/timeline-element.model";
import evalFormula, {
  COMPARATION_VALUE_TYPES,
  CONDITION_TYPES,
} from "./eval-formula";

const ERRORS = {
  ValidationError: "CSVデータが正しくありません。",
} as const;

const xAxisValue = (
  array: string[],
  lineIndex: number,
  axisIndex: number
): string => {
  const row = array[lineIndex];
  const cols = row.split(",");
  return cols[axisIndex];
};

const csvToTimeline = (
  lines: string[],
  xAxisLabel: string,
  conditions: {
    [label: string]: {
      type: typeof COMPARATION_VALUE_TYPES[keyof typeof COMPARATION_VALUE_TYPES];
      comparisonValue: string | number;
      condition: typeof CONDITION_TYPES[keyof typeof CONDITION_TYPES];
    };
  }
) => {
  const timelines: TimelineElement[] = [];

  let continuityFlags: {
    [label: string]: { from: string; last: string } | null;
  } = {};

  const headerLine = lines.shift();
  if (headerLine === undefined) {
    throw new Error(ERRORS.ValidationError);
  }
  const headers = headerLine.split(",");
  for (const label of headers) {
    continuityFlags[label] = null;
  }
  const xAxisLabelIndex = headers.indexOf(xAxisLabel);

  const minNum = Number(xAxisValue(lines, 0, xAxisLabelIndex));
  const maxNum = Number(xAxisValue(lines, lines.length - 1, xAxisLabelIndex));

  if (isNaN(minNum) || isNaN(maxNum)) {
    throw new Error(ERRORS.ValidationError);
  }

  const min = new Date(minNum);
  const max = new Date(maxNum);

  lines.forEach((line, i) => {
    const lineData = line.split(",");

    let j = 0;
    for (const data of lineData) {
      const label = headers[j];

      if (!(label in conditions)) {
        conditions[label] = {
          type: "number",
          comparisonValue: 1,
          condition: ">",
        };
      }

      const result = evalFormula({ ...conditions[label], value: data });

      if (result && continuityFlags[label] === null) {
        // Start
        continuityFlags[label] = {
          from: lineData[xAxisLabelIndex],
          last: lineData[xAxisLabelIndex],
        };
      } else if (!result && continuityFlags[label] !== null) {
        // End
        const from = new Date(Number(continuityFlags[label]!.from));
        let to: Date | number;
        if (isNaN(Number(continuityFlags[label]!.last))) {
          to = from;
        } else {
          to = new Date(Number(continuityFlags[label]!.last));
        }

        timelines.push({
          from,
          to,
          label,
        });

        continuityFlags[label] = null;
      } else if (result && continuityFlags[label] !== null) {
        // Continue
        continuityFlags[label]!.last = lineData[xAxisLabelIndex];
      }

      j++;
    }
  });

  for (const label of Object.keys(continuityFlags)) {
    if (continuityFlags[label] !== null) {
      const from = new Date(Number(continuityFlags[label]!.from));
      let to: Date | number;
      if (isNaN(Number(continuityFlags[label]!.last))) {
        to = from;
      } else {
        to = new Date(Number(continuityFlags[label]!.last));
      }
      timelines.push({
        from,
        to,
        label,
      });
    }
  }

  return {
    min,
    max,
    headers,
    timelines,
    conditions,
  };
};

export default csvToTimeline;
