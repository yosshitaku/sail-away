import { Timeline } from "../../model/timeline.model";
import { VisData } from "../../model/visdata.model";
import { COMPARATION_VALUE_TYPES, CONDITION_TYPES } from "./eval-formula";

type Args = {
  id: string;
  visData: VisData;
  range: { min: number | Date; max: number | Date };
  conditions: {
    [label: string]: {
      type: typeof COMPARATION_VALUE_TYPES[keyof typeof COMPARATION_VALUE_TYPES];
      comparisonValue: string | number;
      condition: typeof CONDITION_TYPES[keyof typeof CONDITION_TYPES];
    };
  };
};

const visDataToMyTimeline = ({
  id,
  visData,
  range,
  conditions,
}: Args): Timeline => {
  const groups = visData.groups.map((g, i) => {
    return { ...g, ...{ value: i } };
  });

  return {
    id,
    items: visData.items,
    groups,
    range,
    conditions,
  };
};

export default visDataToMyTimeline;
