const fs = require('fs');
const os = require("os");

function calcMemory() {
  const memory = {};
  memory.free = os.freemem();
  memory.total = os.totalmem();
  memory.freepercent = (memory.free / memory.total) * 100;

  return memory;
}

function convertUnit(memory, unit) {
  return {
    free: bytesToSize(memory.free),
    total: bytesToSize(memory.total / 1024),
    freepercent: memory.freepercent,
  };
}

function bytesToSize(bytes) {
  var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
  if (bytes === 0) return "0 Byte";
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + " " + sizes[i];
}

function calcCPU() {
  const cpus = os.cpus();

  return cpus.map((cpu) => {
    const usage = {};
    let total = 0;

    for (var type in cpu.times) {
      total += cpu.times[type];
    }

    for (const type in cpu.times) {
      usage[type] = (100 * cpu.times[type]) / total;
    }

    return usage;
  });
}

(() => {

  const filename = 'system_log.csv';
  const header = ["time", "memory-usage"];

  let first = true;
  setInterval(() => {
    const datas = [new Date().getTime()];
    let memory = calcMemory();
    memory = convertUnit(memory);
    datas.push(100 - memory.freepercent);

    const cpus = calcCPU();

    let num = 1;
    for (const cpu of cpus) {
      if (first) {
        header.push(`cpu_${num}`);
      }
      datas.push(
        Object.entries(cpu).reduce((pre, cur) => {
          if (cur[0] === "idle") {
            return pre;
          }
          return pre + cur[1];
        }, 0)
      );
      num++;
    }

    if (first) {
      fs.writeFileSync(filename, header.join(',') + '\n', { encoding: 'utf-8' });
      first = false;
    }

    fs.appendFileSync(filename, datas.join(',') + '\n', { encoding: 'utf-8' });
  }, 1000);
})();
